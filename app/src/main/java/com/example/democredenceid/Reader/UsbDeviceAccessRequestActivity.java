package com.example.democredenceid.Reader;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.democredenceid.Utils.CheckUtils;

public class UsbDeviceAccessRequestActivity extends Activity {

    private static final String TAG = "USB_DEV_ACC_REQ";

    private static final String ACTION_USB_PERMISSION = "cl.autentia.usb.ACTION_USB_PERMISSION";
    private PendingIntent mPermissionIntent;

    public interface Extras {
        String USB_DEVICE = "USB_DEVICE";
    }

    interface State {
        String CURRENT_STATE = "CURRENT_STATE";
    }

    enum InternalState {
        JUST_STARTED(0), WAITING_FOR_PERMISSION(1);

        private final int mCode;

        InternalState(int code) {
            this.mCode = code;
        }
    }

    InternalState mCurrentState = InternalState.JUST_STARTED;
    UsbDevice mUsbDevice = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentState = InternalState.valueOf(savedInstanceState.getString(State.CURRENT_STATE));
        }

        Intent intent = getIntent();
        CheckUtils.isTrue(intent != null).orThrow("intent is null");
        Bundle extras = intent.getExtras();
        CheckUtils.isTrue(extras != null).orThrow("extras is null");
        Object obj = extras.get(Extras.USB_DEVICE);
        if (obj instanceof UsbDevice) {
            mUsbDevice = (UsbDevice) obj;
            Log.d(TAG, "device access requested: " + obj);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(State.CURRENT_STATE, mCurrentState.name());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mCurrentState.equals(InternalState.JUST_STARTED)) {
            Log.d(TAG, "new request...");
            if (mUsbDevice == null) {
                Log.e(TAG, "requested device is null");
                setResult(Activity.RESULT_CANCELED);
                finish();
            } else {
                Log.d(TAG, "requested device: " + mUsbDevice);

                UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
                if (!manager.hasPermission(mUsbDevice)) {
                    Log.d(TAG, "requesting permission...");
                    mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
                    IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
                    registerReceiver(mUsbReceiver, filter);
                    mCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    manager.requestPermission(mUsbDevice, mPermissionIntent);
                } else {
                    Log.d(TAG, "permission already granted...");
                    processDevicePermit(mUsbDevice);
                }
            }
        } else {
            Log.d(TAG, "resuming from request...");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mUsbReceiver);
        } catch (Exception e) {
        }
    }

    protected void processDevicePermit(UsbDevice device) {
        Intent result = null;
        if (device != null) {
            Log.d(TAG, "permission granted for device " + device);
            result = new Intent();
            result.putExtra(Extras.USB_DEVICE, device);
        }
        setResult(Activity.RESULT_OK, result);
        finish();
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                Log.d(TAG, "ACTION_USB_PERMISSION broadcast received");
                synchronized (this) {
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        CheckUtils.isTrue(device != null).orThrow("UsbDevice object is null");
                        processDevicePermit(device);
                    } else {
                        Log.e(TAG, "permission denied for device: " + device);
                        processDevicePermit(null);
                    }
                }
            }
        }
    };


}
