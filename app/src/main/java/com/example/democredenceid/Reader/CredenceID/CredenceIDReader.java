package com.example.democredenceid.Reader.CredenceID;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.hardware.usb.UsbManager;
import android.os.SystemClock;
import android.util.Log;

import com.credenceid.biometrics.Biometrics;
import com.credenceid.biometrics.ConvertToFMDSyncResponse;
import com.example.democredenceid.Reader.FingerprintImage;
import com.example.democredenceid.Reader.FingerprintInfo;
import com.example.democredenceid.Reader.FingerprintReader;

import java.nio.ByteBuffer;
import java.util.logging.Handler;

import cl.autentia.uareu.Globals;

import static com.credenceid.biometrics.Biometrics.FMDFormat.ISO_19794_2_2005;
import static com.credenceid.biometrics.Biometrics.ResultCode.FAIL;
import static com.credenceid.biometrics.Biometrics.ResultCode.INTERMEDIATE;
import static com.credenceid.biometrics.Biometrics.ResultCode.OK;


public class CredenceIDReader extends FingerprintReader {

    private static final String TAG = "CredenceIDReader";
    private Biometrics biometrics;
    private Biometrics.ScanType scanType = Biometrics.ScanType.SINGLE_FINGER;

    private final Biometrics.ScanType[] scanTypes = {
            Biometrics.ScanType.SINGLE_FINGER,
            Biometrics.ScanType.TWO_FINGERS, Biometrics.ScanType.ROLL_SINGLE_FINGER,
            Biometrics.ScanType.TWO_FINGERS_SPLIT
    };


    ScanCallback scanCallback;
    /* global path variables for grabbed fingerprints.*/
    private Bitmap currentBitmap;
    private Bitmap currentFingerprint1Bitmap;
    private Bitmap currentFingerprint2Bitmap;

    private boolean mCaptureFingerprintOne = true;
    /* Stores FMD templates (used for fingerprint matching) for each fingerprint. */
    private byte[] mFingerprintOneFMDTemplate = null;

    private boolean isCapturing = false;
    // If true, grab fingerprint synchronously
    private boolean grabFingerprintSync = false;
    // If true, return the raw byte array of fingerprint which grabbed asynchronously
    private boolean grabFingerprintAsyncRaw = false;
    // Handler for display bitmap which grabbed synchronously
    Handler syncHandler;

    /**
     * The newer API call of grabFingerprint() takes a "onFingerprintGrabbedFullListener" as its
     * listener. This "full" listener can be used on all device types, but to demonstrate the
     * grabFingerprint() API will all three types of listeners, we only this full listener
     * with certain devices.
     */

    private boolean useFingerprintFullListener = true;

    /**
     * The newer API call of grabFingerprint() takes a "onFingerprintGrabbedWSQListener" as its
     * listener. This "wsq" listener can only be used on CredenceTAB/Trident devices. So we only
     * set this flag to true for those device types.
     */

    private boolean useFingerprintWsqListener = false;


    public CredenceIDReader(Activity activity, UsbManager usbManager) {
        super(activity, usbManager);
//        initBiometrics(activity);
    }

    @Override
    public void open() throws Exception {
        Log.e("open()","IN");
        App.BioManager.openFingerprintReader(mFingerprintOpenCloseListener);
    }

    @Override
    public void close() {
        App.BioManager.closeFingerprintReader();
    }

    @Override
    public void cancelCapture() {
        App.BioManager.cancelCapture();
    }

    @Override
    public void capture(ScanCallback scanCallback) throws Exception {
        this.scanCallback = scanCallback;
        captureFingerprintOne();
    }

    @Override
    public void getInfo(InfoCallback infoCallback) throws Exception {

        FingerprintInfo info;
        info = new FingerprintInfo(
                App.BioManager.getDeviceID(),
                App.BioManager.getSDKJarVersion(),
                App.BioManager.getProductName());
        infoCallback.infoResult(info);
    }

    /* Callback invoked every time fingerprint sensor on device opens or closes. */
    private Biometrics.FingerprintReaderStatusListener mFingerprintOpenCloseListener =
            new Biometrics.FingerprintReaderStatusListener() {
                @Override
                public void
                onOpenFingerprintReader(Biometrics.ResultCode resultCode,
                                        String hint) {

                    /* If hint is valid, display it. Regardless of ResultCode we should
                     * message indicating what is going on with sensor.
                     */
                    if (hint != null && !hint.isEmpty())
//                        mStatusTextView.setText(hint);

                        /* This code is returned once sensor has fully finished opening. */
                        if (OK == resultCode) {
                            Log.e("Fingerprint Open: ", hint);
                        }
                        /* This code is returned while sensor is in the middle of opening. */
                        else if (INTERMEDIATE == resultCode) {
                            /* Do nothing while operation is still on-going. */

                            Log.e("Fingerprint Open: ", "INTERMEDIATE");
                        }

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void
                onCloseFingerprintReader(Biometrics.ResultCode resultCode,
                                         Biometrics.CloseReasonCode closeReasonCode) {

                    if (OK == resultCode) {
                        Log.e("Fingerprint Closed: ", closeReasonCode.name());

                        /* Now that sensor is closed, if user presses "mOpenCloseButton" fingerprint
                         * sensor should now open. To achieve this we change flag which controls
                         * what action mOpenCloseButton takes.
                         */

                    } else if (INTERMEDIATE == resultCode) {
                        /* This code is never returned for this API. */

                    } else if (FAIL == resultCode) {
                        Log.e("onCloseFpReader", "Fingerprint FAILED to close.");
                    }
                }
            };



    /* Make CredenceSDK API calls to capture "first" fingerprint. This is fingerprint image on left
     * side of layout file.
     */

    private void
    captureFingerprintOne() {

        mFingerprintOneFMDTemplate = null;

        App.BioManager.grabFingerprintSync(5000);

        ///////////////////////////// this is the method that actually works, the OnFingerprintGrabbedRawListener.

//        App.BioManager.grabFingerprint(scanType, true, new Biometrics.OnFingerprintGrabbedRawListener() {
//            @Override
//            public void onFingerprintGrabbed(Biometrics.ResultCode resultCode, Bitmap bitmap, byte[] bytes, String s, byte[] bytes1, String s1) {
//                /* If a valid hint was given then display it for user to see. */
//                if (s1 != null && !s1.isEmpty())
//                    Log.e("HINT",s1);
//
//                /* This code is returned once sensor captures fingerprint image. */
//                if (OK == resultCode) {
//
//                    Log.e("SAVE IT",s);
//                    byte [] bits = new byte[bytes1.length*4]; //That's where the RGBA array goes.
//                    int i;
//                    for(i=0;i<bytes1.length;i++){
//                        bits[i*4] =
//                                bits[i*4+1] = bits[i*4+2] = (byte) ~bytes1[i]; //Invert the source bits
//                                bits[i*4+3] = (byte) 0xff; // the alpha.
//                    }
//
//                    Bitmap bm = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
//                    bm.copyPixelsFromBuffer(ByteBuffer.wrap(bits));
//
//                    Bitmap bmp = Globals.GetBitmapFromRaw(bytes1,bm.getWidth(),bm.getHeight());
//
//                    scanCallback.scanResult(new FingerprintImage(bytes1, bmp.getWidth(), bmp.getHeight(), bmp.getDensity()));
//                }
//
//                /* This code is returned on every new frame/image from sensor. */
//                else if (INTERMEDIATE == resultCode) {
//                    /* This hint is returned if cancelCapture()" or "closeFingerprint()" are called
//                     * while in middle of capture.
//                     */
//                    if (s1 != null && s1.equals("Capture Stopped"))
//                        Log.e("INTERMEDIATE","Capture Stopped");
//                }
//                /* This code is returned if sensor fails to capture fingerprint image. */
//                else if (FAIL == resultCode) {
//                    Log.e("FAIL","Failed to capture");
//                }
//            }
//
//            @Override
//            public void onCloseFingerprintReader(Biometrics.ResultCode resultCode, Biometrics.CloseReasonCode closeReasonCode) {
//
//            }
//        });

        ///////////////////////////// this is a test Method.

//        App.BioManager.grabFingerprint(scanType,true, new Biometrics.OnFingerprintGrabbedListener() {
//            @Override
//            public void onFingerprintGrabbed(Biometrics.ResultCode resultCode, Bitmap bitmap, byte[] bytes, String s, String s1) {
//
//                /* If a valid hint was given then display it for user to see. */
//                if (s1 != null && !s1.isEmpty())
//                    Log.e("HINT",s1);
//
//                /* This code is returned once sensor captures fingerprint image. */
//                if (OK == resultCode) {
//                    Log.e("SAVE IT",s);
////                    if (bytes != null)
//                        if (null != bitmap)
////                            createFMDTemplate(bitmap);
//                            scanCallback.scanResult(new FingerprintImage(bytes, bitmap.getWidth(), bitmap.getHeight(), bitmap.getDensity()));
//                }
//                /* This code is returned on every new frame/image from sensor. */
//                else if (INTERMEDIATE == resultCode) {
//                    /* This hint is returned if cancelCapture()" or "closeFingerprint()" are called
//                     * while in middle of capture.
//                     */
//                    if (s1 != null && s1.equals("Capture Stopped"))
//                        Log.e("INTERMEDIATE","Capture Stopped");
//                }
//                /* This code is returned if sensor fails to capture fingerprint image. */
//                else if (FAIL == resultCode) {
//                    Log.e("FAIL","Failed to capture");
//                }
//            }
//
//            @Override
//            public void onCloseFingerprintReader(Biometrics.ResultCode resultCode, Biometrics.CloseReasonCode closeReasonCode) {
//
//            }
//        });

        ///////////////////////////// this is the method that makes the app fail.

        App.BioManager.grabFingerprint(scanType,true, new Biometrics.OnFingerprintGrabbedWSQListener() {
            @Override
            public void onFingerprintGrabbed(Biometrics.ResultCode resultCode, Bitmap bitmap, byte[] bytes, String s, String s1, String s2, int ii) {
                /* If a valid hint was given then display it for user to see. */
                if (s2 != null && !s2.isEmpty())
                    Log.e("HINT",s2);

                /* This code is returned once sensor captures fingerprint image. */
                if (OK == resultCode) {
                    Log.e("SAVE IT",s);
                    Log.e("SAVE IT",s1);
//                    if (bytes != null)
                        if (null != bitmap){
                            byte [] bits = new byte[bytes.length*4]; //That's where the RGBA array goes.
                            int i;
                            for(i=0;i<bytes.length;i++){
                                bits[i*4] =
                                        bits[i*4+1] = bits[i*4+2] = (byte) ~bytes[i]; //Invert the source bits
                                        bits[i*4+3] = (byte) 0xff; // the alpha.
                            }

                            Bitmap bm = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
                            bm.copyPixelsFromBuffer(ByteBuffer.wrap(bits));

                            Bitmap bmp = Globals.GetBitmapFromRaw(bytes,bm.getWidth(),bm.getHeight());

                            scanCallback.scanResult(new FingerprintImage(bytes, bmp.getWidth(), bmp.getHeight(), bmp.getDensity()));
                        }
//                            createFMDTemplate(bitmap); //todo: if we can use the bitmap (with 500dpi), next step is to use the createFMDTemplate to see if we can get a valid match with the return of this method,
//                                                          just to see if it works with the ISO_19794_2_2005 standard.
                }
                /* This code is returned on every new frame/image from sensor. */
                else if (INTERMEDIATE == resultCode) {
                    /* This hint is returned if cancelCapture()" or "closeFingerprint()" are called
                     * while in middle of capture.
                     */
                    if (s2 != null && s2.equals("Capture Stopped"))
                        Log.e("INTERMEDIATE","Capture Stopped");
                }
                /* This code is returned if sensor fails to capture fingerprint image. */
                else if (FAIL == resultCode) {
                    Log.e("FAIL","Failed to capture");
                }
            }

            @Override
            public void onCloseFingerprintReader(Biometrics.ResultCode resultCode, Biometrics.CloseReasonCode closeReasonCode) {
                if (resultCode == OK) {
//                    setStatusText("FingerPrint reader closed:" + closeReasonCode.toString());
//                    resetCapture();
                } else if (resultCode == Biometrics.ResultCode.FAIL) {
                    /* If sensor failed to close, then close button should still be clickable
                     * since it did not actually close.
                     */
//                    updateToCloseButton();
                    Log.e(TAG,"FingerPrint reader closed: FAILED");
                }
            }
        });
    }

    /* Attempts to create a FMD template from given Bitmap image. If successful it saves FMD to
     * respective fingerprint template array.
     *
     * @param bitmap
     */
    @SuppressLint("SetTextI18n")
    private void
    createFMDTemplate(Bitmap bitmap) {

        /* Keep a track of how long it takes for FMD creation. */
        final long startTime = SystemClock.elapsedRealtime();

        App.BioManager.convertToFMD(bitmap, ISO_19794_2_2005, (Biometrics.ResultCode resultCode,
                                                               byte[] bytes) -> {

            if (OK == resultCode) {

                /* Display how long it took for FMD template to be created. */
                final double durationInSeconds = (SystemClock.elapsedRealtime() - startTime) / 1000.0;
                Log.e("TIMETAKED","Created FMD template in: " + durationInSeconds + " seconds.");

                if (mCaptureFingerprintOne) {
//                    mFingerprintOneFMDTemplate = Arrays.copyOf(bytes, bytes.length);
                    Log.e("INFO","bitmap width"+ bitmap.getWidth());
                    Log.e("INFO","bitmap height"+ bitmap.getHeight());
                    Log.e("INFO","bitmap density"+ bitmap.getDensity());
                    byte [] bits = new byte[bytes.length*4]; //That's where the RGBA array goes.
                    int i;
                    for(i=0;i<bytes.length;i++)
                    {
                        bits[i*4] =
                                bits[i*4+1] = bits[i*4+2] = (byte) ~bytes[i]; //Invert the source bits
                        bits[i*4+3] = (byte) 0xff; // the alpha.
                    }
                    //Now put these nice RGBA pixels into a Bitmap object

                    Bitmap bm = Bitmap.createBitmap(1000, 1000, Bitmap.Config.ARGB_8888);
                    ByteBuffer buffer = ByteBuffer.wrap(bits);
                    buffer.rewind();
                    bm.copyPixelsFromBuffer(buffer);
                    if(mFingerprintOneFMDTemplate != null) scanCallback.scanResult(new FingerprintImage(mFingerprintOneFMDTemplate, bm.getWidth(), bm.getHeight(), bm.getDensity()));
                }

            } else if (INTERMEDIATE == resultCode) {
                /* This code is never returned for this API. */

            } else if (FAIL == resultCode) {
                Log.e("FAIL","Failed to create FMD template.");
            }
        });
    }

    private static final int SYNC_API_TIMEOUT_MS = 3000;

    private byte[]
    convertWSQToFMDSync(byte[] WSQ) {

        ConvertToFMDSyncResponse res = App.BioManager.convertToFMDSync(WSQ,
                ISO_19794_2_2005,
                SYNC_API_TIMEOUT_MS);

        if (null != res && OK == res.resultCode)
            return res.FMD;
        return new byte[]{};
    }

}
