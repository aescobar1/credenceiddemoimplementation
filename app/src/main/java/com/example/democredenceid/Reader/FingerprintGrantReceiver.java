package com.example.democredenceid.Reader;

public interface FingerprintGrantReceiver {

    void onFingerprintAccessGranted();

    void onFingerprintAccessDenied();

    void onFingerprintAccessError();
}
