package com.example.democredenceid.Reader;

import android.app.Activity;
import android.hardware.usb.UsbManager;

public abstract class FingerprintReader {

    public FingerprintReader(Activity activity, UsbManager usbManager) {
    }

    public boolean isOpen;

    public abstract void open() throws Exception;

    public abstract void close();

    public abstract void cancelCapture();

    public abstract void capture(ScanCallback scanCallback) throws Exception;

    public abstract void getInfo(InfoCallback infoCallback) throws Exception;

    public interface ScanCallback {
        void scanResult(FingerprintImage image);
    }

    public interface InfoCallback {
        void infoResult(FingerprintInfo info);
    }


}
