package com.example.democredenceid.Reader;

public class FingerprintInfo {

    public FingerprintInfo(String serialNumber, String version, String readerType) {
        this.serialNumber = serialNumber;
        this.version = version;
        this.readerType = readerType;
    }

    public String serialNumber;
    public String version;
    public String readerType;
}
