package com.example.democredenceid.Reader.CredenceID;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.democredenceid.Reader.FingerprintGrantReceiver;
import com.example.democredenceid.Reader.FingerprintManager;
import com.example.democredenceid.Reader.FingerprintReader;
import com.example.democredenceid.Reader.UsbDeviceAccessRequestActivity;

import java.util.Collection;

public class CredenceIDManager extends FingerprintManager {


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean isUsbPresent() throws Exception {
        UsbDevice aDevice = findSuitableDevice();
        return aDevice != null;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean isUsbAccessible() {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        UsbDevice aDevice = null;
        try {
            aDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        return manager.hasPermission(aDevice);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void requestAccess(int requestCode) {

        Intent intent = new Intent(mActivity, UsbDeviceAccessRequestActivity.class);
        UsbDevice aDevice = null;
        try {
            aDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        intent.putExtra(UsbDeviceAccessRequestActivity.Extras.USB_DEVICE, aDevice);
        mActivity.startActivityForResult(intent, requestCode);

    }

    @Override
    public void processAccessGrant(int resultCode, Intent result, FingerprintGrantReceiver receiver) {
        if (resultCode == Activity.RESULT_OK) {

            if (result == null) {
                receiver.onFingerprintAccessDenied();
                return;
            }
            Parcelable authorizedDevice = result.getParcelableExtra(
                    UsbDeviceAccessRequestActivity.Extras.USB_DEVICE);

            if (authorizedDevice != null) {
                receiver.onFingerprintAccessGranted();
            } else {
                receiver.onFingerprintAccessDenied();
            }
        } else {
            receiver.onFingerprintAccessError();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public FingerprintReader getReader(Activity activity) {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        CredenceIDReader theReader = new CredenceIDReader(activity, manager);
        UsbDevice suitableDevice = null;
        try {
            suitableDevice = findSuitableDevice();
        } catch (Exception e) {
        }
        if (suitableDevice != null) {
            return theReader;
        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public UsbDevice findSuitableDevice() throws Exception {
        UsbManager manager = (UsbManager) mActivity.getSystemService(Context.USB_SERVICE);
        Collection<UsbDevice> deviceList = manager.getDeviceList().values();
        for (UsbDevice aDevice : deviceList) {
            Log.e("getDeviceName",aDevice.getDeviceName());
            Log.e("getManufacturerName",aDevice.getManufacturerName());
            Log.e("getProductName",aDevice.getProductName());
            Log.e("getDeviceId",""+aDevice.getSerialNumber());
            if (deviceList.size()==1)
                return aDevice;
            else
                Log.e("findsuitabledevice","more than one device");
        }
        throw new Exception("Error en configuración del huellero");
    }
}
