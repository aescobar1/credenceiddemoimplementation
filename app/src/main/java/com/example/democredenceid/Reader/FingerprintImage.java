package com.example.democredenceid.Reader;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.digitalpersona.uareu.UareUException;
import com.example.democredenceid.Utils.ImageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class FingerprintImage implements Parcelable {

    protected int mWidth;
    protected int mHeight;
    protected int mDPI = 500;
    protected byte[] mRawPixels;
    private byte[] mWsqImage;
    private float mWsqBitrate = 0.0f;
    protected Bitmap mBitmap;
    private int mQuality = -1;

    protected FingerprintImage(Parcel in) {
        mWidth = in.readInt();
        mHeight = in.readInt();
        mDPI = in.readInt();
        mRawPixels = in.createByteArray();
        mQuality = in.readInt();
    }

    public static final Creator<FingerprintImage> CREATOR = new Creator<FingerprintImage>() {
        @Override
        public FingerprintImage createFromParcel(Parcel in) {
            return new FingerprintImage(in);
        }

        @Override
        public FingerprintImage[] newArray(int size) {
            return new FingerprintImage[size];
        }
    };

    public static FingerprintImage fromWSQ(byte[] wsq) {
        return null;
    }

    public FingerprintImage(byte[] rawPixels, int width, int height, int dpi) {
        mRawPixels = rawPixels;
        mWidth = width;
        mHeight = height;
//        mDPI = dpi;
        mDPI = 500;
    }

    public FingerprintImage() {
    }

    public byte[] getRawImage() {
        return mRawPixels;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public byte[] getImageWSQ() throws IOException, UareUException {
        if (mWsqImage == null) {
            mWsqImage = ImageUtils.rawToWSQ(mRawPixels, mWidth, mHeight, mDPI, 8);
//            Writer output = null;
//            File file = new File(Environment.getExternalStorageDirectory()+"/AutentiaMovil/CredenceID_WSQ.wsq");
//            output = new BufferedWriter(new FileWriter(file));
//            output.write(Base64.encodeToString(mWsqImage,Base64.DEFAULT));
//            output.close();
        }
        return mWsqImage;
    }

    public FingerprintImage getRotated180() {
        byte[] rotated = new byte[mRawPixels.length];
        for (int i = 0; i < mRawPixels.length; i++) {
            rotated[rotated.length - i - 1] = mRawPixels[i];
        }
        return new FingerprintImage(rotated, mWidth, mHeight, mDPI);
    }

    public Bitmap getBitmap() {
        if (mBitmap == null) {
            byte[] rgbaPixels = new byte[mWidth * mHeight * 4];
            for (int i = 0; i < mWidth * mHeight; i++) {
                rgbaPixels[i * 4] = rgbaPixels[i * 4 + 1] = rgbaPixels[i * 4 + 2] = mRawPixels[i];
                rgbaPixels[i * 4 + 3] = -1;
            }
            mBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ARGB_8888);
            mBitmap.copyPixelsFromBuffer(ByteBuffer.wrap(rgbaPixels));
        }
        return mBitmap;
    }

    public FingerprintImage scaleToDPI(int dpi) {
        float scaleFactor = (float) dpi / mDPI;
        Bitmap tmpBitmap = Bitmap.createBitmap(mWidth, mHeight, Bitmap.Config.ALPHA_8);
        tmpBitmap.copyPixelsFromBuffer(ByteBuffer.wrap(mRawPixels));
        Bitmap scaledResult = tmpBitmap.createScaledBitmap(tmpBitmap, (int) (mWidth * scaleFactor), (int) (mHeight * scaleFactor), true);
        try {
            byte[] pixels = new byte[scaledResult.getWidth() * scaledResult.getHeight()];
            int pixel = 0;
            for (int y = 0; y < scaledResult.getHeight(); y++) {
                for (int x = 0; x < scaledResult.getWidth(); x++, pixel++) {
                    pixels[pixel] = (byte) scaledResult.getPixel(x, y);
                }
            }
            return new FingerprintImage(pixels, scaledResult.getWidth(), scaledResult.getHeight(), dpi);
        } finally {
            tmpBitmap.recycle();
            scaledResult.recycle();
        }
    }

    public Bitmap getCroppedBitmap(int width, int height) {

        int[] pixels = new int[width * height];

        int row = 0;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int grey = mRawPixels[row + x];
                pixels[row + x] = 0xFF000000 | (grey * 0x00010101);
            }
            row += width;
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public boolean savePGM(String path) {
        File imageFile = new File(path);
        try {
            FileOutputStream out = new FileOutputStream(imageFile);
            String header = String.format("P5\n%d %d\n255\n", mWidth, mHeight);
            out.write(header.getBytes(Charset.forName("UTF-8")));
            out.write(mRawPixels);
            out.close();
            return true;
        } catch (java.io.IOException e) {
            return false;
        }
    }

    public void setQuality(int quality) {
        mQuality = quality;
    }

    public int getQuality() {
        return mQuality;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mWidth);
        dest.writeInt(mHeight);
        dest.writeInt(mDPI);
        dest.writeByteArray(mRawPixels);
        dest.writeInt(mQuality);
    }

    public int getDpi() {
        return mDPI;
    }
}
