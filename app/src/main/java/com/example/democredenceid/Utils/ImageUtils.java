package com.example.democredenceid.Utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;

import com.digitalpersona.uareu.Compression;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.Arrays;

public class ImageUtils {

    /**
     *
     * @param rawImage
     * @param width
     * @param height
     * @param dpi
     * @param bpp
     * @return
     * @throws IOException
     * @throws UareUException
     */
    public static byte[] rawToWSQ(byte[] rawImage, int width, int height,
                                  int dpi, int bpp) throws IOException, UareUException {

        byte[] wsq = null;

        Compression compression = UareUGlobal.GetCompression();
        try {
            compression.Start();
            compression.SetWsqBitrate(200, 0);
            wsq = compression.CompressRaw(rawImage, width, height, dpi, bpp, Compression.CompressionAlgorithm.COMPRESSION_WSQ_NIST);

        } catch (Throwable e) {
            throw e;
        } finally {
            compression.Finish();
        }
        return wsq;
    }

    public static byte[] wsqToRAW(byte[] wsq, int width, int height,
                                  int dpi, int bpp) throws IOException, UareUException {

        Compression.RawImage raw = null;

        Compression compression = UareUGlobal.GetCompression();
        try {
            compression.Start();
            raw = compression.ExpandRaw(wsq,Compression.CompressionAlgorithm.COMPRESSION_WSQ_NIST);

        } catch (Throwable e) {
            throw e;
        } finally {
            compression.Finish();
        }
        return raw.data;
    }

    public static int[] imageHistogram(byte[] input, int width, int height) {

        int[] histogram = new int[256];
        Arrays.fill(histogram, 0);

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int gray = input[width * j + i] & 0xff;
                histogram[gray]++;
            }
        }
        return histogram;
    }

    public static byte[] greyHistogramEqualization(byte[] image, int width,
                                                   int height) {
        int gray;
        // Get the Lookup table for histogram equalization
        int[] histLUT = histogramEqualizationLUT(image, width, height);

        byte[] histogramEQ = new byte[image.length];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                // Get pixels by R, G, B
                gray = image[j * width + i] & 0xff;

                // Set new pixel values using the histogram lookup table
                gray = histLUT[gray];

                histogramEQ[j * width + i] = (byte) gray;
            }
        }

        return histogramEQ;
    }

    private static int[] histogramEqualizationLUT(byte[] image, int width,
                                                  int height) {

        // Get an image histogram - calculated values by R, G, B channels
        int[] imageHist = imageHistogram(image, width, height);

        // Fill the lookup table
        int[] ghistogram = new int[256];

        for (int i = 0; i < ghistogram.length; i++)
            ghistogram[i] = 0;

        long sumg = 0;

        // Calculate the scale factor
        float scale_factor = (float) (255.0 / (width * height));

        for (int i = 0; i < ghistogram.length; i++) {
            sumg += imageHist[i];
            int valg = (int) (sumg * scale_factor);
            if (valg > 255) {
                ghistogram[i] = 255;
            } else
                ghistogram[i] = valg;
        }

        return ghistogram;

    }

    public static Mat grayByteToMat(byte[] data, int width, int height) {
        Mat mat = new Mat(height, width, CvType.CV_8U);
        mat.put(0, 0, data);
        return mat;
    }

    public static Bitmap grayByteToBitmap(byte[] data, int width, int height) {
        int[] pixels = new int[width * height];
        byte[] yuv = data;
        int row = 0;
        try {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int grey = yuv[row + x] & 0xff;
                    pixels[row + x] = 0xFF000000 | (grey * 0x00010101);
                    // pixels[row + x] = yuv[row + x];
                }
                row += width;
            }
        } catch (Exception e) {
            if (e.getMessage() != null) {
                Log.e("grayByteToBitmap", e.getMessage());
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static byte[] bitmapToGrayByte(Bitmap bitmap) {
        Bitmap picture = bitmap;
        int width = picture.getWidth();
        int height = picture.getHeight();
        int bytes = width * height;

        IntBuffer buffer = IntBuffer.allocate(bytes);
        picture.copyPixelsToBuffer(buffer);
        int[] array = buffer.array();

        byte grayscale[] = new byte[width * height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = array[i + (j * width)];
                // int a = pixel >>> 24 ;
                int r = (pixel >> 16) & 0xFF;
                int g = (pixel >> 8) & 0xFF;
                int b = pixel & 0xFF;
                int grey = (r + g + b) / 3;
                grayscale[i + (j * width)] = (byte) grey;
            }
        }
        return grayscale;
    }

    public static byte[] BitmapToPNG(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, baos);
        return baos.toByteArray();
    }

    public static byte[] decompressKCA(byte[] kca) {
        byte[] raw = new byte[92160];
        int[] KCAData = new int[8];
        int RawData, lastData = 128;
        int count, KCA = 0;
        count = RawData = 0;

        int cnt = 0;

        do {
            // KCA = kca[cnt++] & 0xFF;
            // KCA += (kca[cnt++] & 0xFF) * 256;
            // KCA += (kca[cnt++] & 0xFF) * 256 * 256;
            KCA = ((kca[cnt++] & 0xff)) | ((kca[cnt++] & 0xff) << 8)
                    | ((kca[cnt++] & 0xff) << 16);
            for (int x = 0; x < KCAData.length; x++) {
                KCAData[x] = KCA & 0x07;
                KCA /= 8;
            }

            // reconstruct the data...
            for (int BlkCnt = 0; BlkCnt < KCAData.length; BlkCnt++) {
                switch (KCAData[BlkCnt]) {
                    case 0:
                        RawData = (lastData - 64);
                        break;
                    case 1:
                        RawData = (lastData - 22);
                        break;
                    case 2:
                        RawData = (lastData - 8);
                        break;
                    case 3:
                        RawData = (lastData - 3);
                        break;
                    case 4:
                        RawData = (lastData + 3);
                        break;
                    case 5:
                        RawData = (lastData + 8);
                        break;
                    case 6:
                        RawData = (lastData + 22);
                        break;
                    case 7:
                        RawData = (lastData + 64);
                        break;
                    default:
                        // dataIn.append("Data > 7");
                }
                lastData = RawData;
                if (count == 92159) {
                    break;
                }
                raw[count++] = (byte) (lastData & 0xff);
            }
        } while (cnt < kca.length);

        return raw;
    }

//    public static Mat expand(Mat source, int newWidth, int newHeight) {
//        Mat dest = new Mat(newHeight, newWidth, source.type());
//        dest.setTo(new Scalar(0));
//        Imgproc.copyMakeBorder(source, dest, 0, newHeight - source.height(),
//                newWidth - source.width(), 0, Imgproc.BORDER_CONSTANT,
//                new Scalar(0));
//        return dest;
//    }

    public static Bitmap unsharpMask(Bitmap imageOrigen) {

        Mat image = new Mat();
        Utils.bitmapToMat(imageOrigen, image);

        Mat tmp = new Mat();

        Imgproc.GaussianBlur(image, tmp, new Size(5, 5), 5);
        Core.addWeighted(image, 1.5, tmp, -0.5, 0, image);

        Bitmap retorno = android.graphics.Bitmap.createBitmap(image.width(),
                image.height(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(image, retorno);
        return retorno;
    }

    public static byte[] unsharpMask(byte[] image, int width, int height) {
        Bitmap retorno = ImageUtils.unsharpMask(ImageUtils.grayByteToBitmap(
                image, width, height));
        return ImageUtils.bitmapToGrayByte(retorno);
    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    public static Bitmap getResizedBitmap(Bitmap bm, float scale) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scale, scale);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }

    public static Bitmap getResizedBitmapInverted(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();

        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postRotate(180);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);
        return resizedBitmap;
    }


}
