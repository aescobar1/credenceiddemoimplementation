package com.example.democredenceid;

public enum ReturnCode implements ReturnCodeStructure {


    //Genericos
    EXITO(200, "Proceso finalizado correctamente"),
    ERROR_GENERICO(201, "%s"),
    SIN_AUDITORIAS_RESERVADAS(202, "Sin auditorias reservadas"),
    RUT_INVALIDO(203, "Rut inválido"),
    CODIGO_BARRAS_INVALIDO(204, "Código de barras inválido"),
    DISPOSITIVO_SIN_NFC(205, "Dispositivo sin NFC"),
    FALLO_APERTURA_NFC(206, "Fallo la apertura de la interfaz NFC"),
    NFC_TIMEOUT(207, "TimeOut"),
    PARAMETROS_INVALIDOS(208, "Parámetros inválidos"),
    HUELLERO_NO_REGISTRADO(209, "El lector no se encuentra registrado"),
    ACCESO_NFC_DENEGADO(210, "Acceso NFC denegado"),
    ACCESO_HUELLERO_DENEGADO(211, "Acceso Huellero denegado"),
    ERROR_EN_PEDIDO_DE_PERMISO_NFC(212, "Error en pedido de permiso NFC"),
    ERROR_EN_PEDIDO_DE_PERMISO_HUELLERO(213, "Error en pedido de permiso Huellero"),
    ERROR_HUELLERO_DESCONECTADO(214, "Huellero desconectado"),
    DATOS_FALTANTES(288, "Código de barras inválido | Rut - Serie faltantes"),
    VERIFICACION_CANCELADA(289, "Verificacion cancelada"),


    //CaptureEvidenceActivity
    CAPTURE_EVIDENCIA_INDICAR_DEDO_A_CAPTURAR(301, "Debe indicar el dedo a capturar"),

    //BrowseableActivity
    BROWSEABLE_LLAMADA_INVALIDA(401, "Llamada inválida"),
    BROWSEABLE_SIN_DATOS(402, "Sin datos"),
    BROWSEABLE_SIN_OPERACION(403, "Sin operación"),

    //SaveDataActivity
//    SAVE_DATA_ERROR_INSERCION_DATOS(501, "Error, no se realizó la inserción de los datos: %s"),

    //PhotoActivity
    PHOTO_ERROR_CAPTURA(601, "Error en captura de foto"),

    //InstitutionPropertiesActivity
    INSTITUTION_PROP_FALTA_SENDER(701, "Falta el parámetro \"Sender\""),
    INSTITUTION_PROP_SIN_PROPERTIES(702, "Sin properties asociadas"),

    //InfoNFCActivity
    ERROR_CEDULA_NO_VIGENTE(215, "Cedula no vigente"),
    ERROR_CEDULA_BLOQUEADA(216, "Cedula con bloqueo"),
    ERROR_CEDULA_BLOQUEADA_TEMPORAL(217, "Cedula con bloqueo temporal"),
    ERROR_SERVICIO_EN_CONTINGENCIA(218, "Cedula vigente | Servicio en contingencia"),

    //VerificationActivity
    VERIFICATION_RUT_SIN_HUELLAS_DISPONIBLES(901, "Rut sin huellas disponibles"),
    VERIFICATION_SIN_INTERNET(902, "La verificación requiere conexión a internet."),
    VERIFICATION_SUPERADO_INTENTOS_VERIFICACION(903, "Ha superado el máximo de intentos de verificación"),
    VERIFICATION_RUT_NO_ENROLADO(904, "Rut no enrolado"),
    VERIFICATION_DISPOSITIVO_NO_CONECTADO(905, "Dispositivo no conectado"),
    VERIFICATION_ERROR_CARGA_DRIVER(906, "Error al cargar driver de Verificación"),
    VERIFICATION_ERROR_CARGA_CONTENIDO_LEGAL(909, "Error al cargar contenido legal (términos y condiciones)"),
    VERIFICATION_RUT_CAPTURADO_NO_COINCIDE_CON_CEDULA(910, "%s"),
    VERIFICATION_ERROR_OBTENCION_DEDO_DESDE_CODIGO_BARRAS(911, "Error al obtener dedo codificado desde código de barras"),
    VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES(912, "No aceptación de términos y condiciones"),
    VERIFICACION_PROBLEMAS_DE_SERVICIO_HUELLAS(913, "Error al obtener las huellas"),

    //transunion
    NO_QUESTIONS_FOR_RUT_TRANSUNION(3000, "No existen preguntas asociadas al rut."),
    ERROR_GENERICO_TRANSUNION(3001, "%s"),
    VERIFICATION_SIN_INTERNET_TRANSUNION(3002, "Problemas de conexión con el servicio"),
    VERIFICATION_NO_ACEPTACION_TERMINOS_Y_CONDICIONES_TRANSUNION(3003, "No aceptación de términos y condiciones"),

    //volleyHelper returncodes
    INVALID_TOKEN(9000,"%s"), //todo: check string message
    CANT_GET_AUDIT(9001,"Error de acceso, no se pueden obtener auditorias reservadas."),
    VOLLEY_TIMEOUT(9002,"Se ha superado el tiempo de respuesta de la petición o no es posible conectar con el servidor."), //todo: check string message
    VOLLEY_SERVER(9003,"Error interno del servidor."), //todo: check string message
    VOLLEY_NETWORK(9004,"No es posible realizar la conexión con el servicio."), //todo: check string message
    VOLLEY_PARSE(9005,"Error de parse de la data obtenida, el formato de respuesta del servicio no es válido.") //todo: check string message

    ;

    private int code;
    private String description;

    ReturnCode(int code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public int getCode() {
        return this.code;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
