package com.example.democredenceid;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.credenceid.biometrics.Biometrics;
import com.credenceid.biometrics.BiometricsManager;
import com.example.democredenceid.Reader.CredenceID.App;
import com.example.democredenceid.Reader.FingerprintGrantReceiver;
import com.example.democredenceid.Reader.FingerprintImage;
import com.example.democredenceid.Reader.FingerprintInfo;
import com.example.democredenceid.Reader.FingerprintManager;
import com.example.democredenceid.Reader.FingerprintReader;
import com.example.democredenceid.Utils.ImageUtils;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.UiThread;


@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity  implements FingerprintReader.ScanCallback, FingerprintReader.InfoCallback, FingerprintGrantReceiver {

    private static final int REQUEST_FINGERPRINT_ACCESS = 0x031;
    private FingerprintManager mFingerprintManager;
    private FingerprintReader mFingerprintReader;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFingerprintManager = FingerprintManager.getInstance(this);
//        mAutentiaPreferences = new AutentiaPreferences(this);
        if (mFingerprintManager.getClass().getName().equals("com.example.democredenceid.Reader.CredenceID.CredenceIDManager") && !openBiometrics) {
            initBiometrics(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mFingerprintManager.getClass().getName().equals("com.example.democredenceid.Reader.CredenceID.CredenceIDManager") && openBiometrics) {
            Log.e("asdf","asdf");
            App.BioManager.cancelCapture();
            App.BioManager.closeFingerprintReader();
            App.BioManager = null;
            openBiometrics = false;
        }
    }

    @InstanceState
    InternalState mFingerprintCurrentState = InternalState.JUST_STARTING;
    private FingerprintInfo mFingerprintInfo;
    private boolean openBiometrics= false;

    @Override
    public void onFingerprintAccessGranted() {
        mFingerprintReader = mFingerprintManager.getReader(this);
        startCaptureFingerprint();
    }

    @Override
    public void onFingerprintAccessDenied() {
        showMessage("Acceso al huellero denegado");
    }

    @Override
    public void onFingerprintAccessError() {
        showMessage("Error de permisos del huellero");
    }

    @Override
    @org.androidannotations.annotations.UiThread(propagation = UiThread.Propagation.REUSE)
    public void scanResult(FingerprintImage image) {
        dismissProgress();
        drawFingerPrintImage(image);
    }

    @Override
    public void infoResult(FingerprintInfo info) {


        try {

            mFingerprintInfo = info;
            mFingerprintReader.capture(this);
        } catch (Exception e) {
            showMessage(e.getMessage());
        }

    }

    @OnActivityResult(value = REQUEST_FINGERPRINT_ACCESS)
    protected void onRequestFingerprintAccessResult(int resultCode, Intent data) {
        mFingerprintManager.processAccessGrant(resultCode, data, this);
    }

    public void huellerosTest(View view) {
        testHuella();
    }

    private void testHuella(){
        try {

            if (mFingerprintCurrentState.equals(InternalState.JUST_STARTING)) {
                if (!mFingerprintManager.isUsbPresent()) {
                    throw new AutentiaMovilException(Status.ERROR, ReturnCode.VERIFICATION_DISPOSITIVO_NO_CONECTADO);
                }
                if (mFingerprintManager.isUsbAccessible()) {
                    mFingerprintCurrentState = InternalState.READY;
                    mFingerprintReader = mFingerprintManager.getReader(this);
                    startCaptureFingerprint();

                } else {
                    mFingerprintCurrentState = InternalState.WAITING_FOR_PERMISSION;
                    mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                }
            } else if (mFingerprintCurrentState.equals(InternalState.WAITING_FOR_PERMISSION)) {
                mFingerprintManager.requestAccess(REQUEST_FINGERPRINT_ACCESS);
                // permiso llegara mediante onActivityResult
            } else if (mFingerprintCurrentState.equals(InternalState.READY)) {
                // tengo impresion que nunca deberiamos llegar aqui
                mFingerprintReader = mFingerprintManager.getReader(this);
                startCaptureFingerprint();
            }
        } catch (AutentiaMovilException e) {
            showMessage(e.returnCode.getDescription());

        } catch (Exception e) {
            showMessage(e.toString());
        }
    }

    private void startCaptureFingerprint() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle("Test de captura de huella ");
        mProgressDialog.setMessage("Coloque el dedo en el lector...");
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                mFingerprintReader.cancelCapture();
                mFingerprintReader.close();
                finish();
            }
        });
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
        getInfo();
    }

    @Background
    void getInfo() {
        try {
            mFingerprintReader.getInfo(this);
        } catch (Exception e) {
            showMessage(e.getMessage());
        }
    }

    private void drawFingerPrintImage(FingerprintImage pfingerprintImage) {
        if (pfingerprintImage != null) {
            View view = getLayoutInflater().inflate(R.layout.test_huella_layout,null);
            ImageView ivFinger = view.findViewById(R.id.iv_finger);
            ivFinger.setImageBitmap(ImageUtils.getResizedBitmapInverted(pfingerprintImage.getBitmap()));
            TextView tvCopiar = view.findViewById(R.id.tv_copiar);
            tvCopiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", mFingerprintInfo.serialNumber);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(MainActivity.this, String.format("Nro. de serie copiado en portapapeles"), Toast.LENGTH_SHORT).show();
                }
            });
            new AlertDialog.Builder(this)
                    .setTitle("Resultado")
                    .setMessage(String.format("Nro. Serie:\n\n%s", mFingerprintInfo.serialNumber))
                    .setCancelable(false)
                    .setPositiveButton("Reintentar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            dialog.cancel();
                            testHuella();
                        }
                    })
                    .setNegativeButton("Aceptar", null)
                    .setView(view)
                    .create()
                    .show();
        }
    }

    @org.androidannotations.annotations.UiThread(propagation = org.androidannotations.annotations.UiThread.Propagation.REUSE)
    void dismissProgress() {
        mProgressDialog.dismiss();
    }


    public void onFinish(View view) {

        if (mFingerprintManager.getClass().getName().equals("com.example.democredenceid.Reader.CredenceID.CredenceIDManager") && openBiometrics) {
            App.BioManager.closeFingerprintReader();
            App.BioManager = null;
            openBiometrics = false;
        }
        finish();

    }

    @org.androidannotations.annotations.UiThread(propagation = org.androidannotations.annotations.UiThread.Propagation.REUSE)
    void showMessage(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setTitle("Resultado:")
                .setCancelable(false)
                .setPositiveButton("Aceptar", null)
                .create().show();
    }

    /**
     *
     * CredenceID Implementation
     * */


    @SuppressLint("LongLogTag")
    private void
    initBiometrics(Activity activity) {

        /*  Create new biometrics object. */
        if(App.BioManager == null){
            App.BioManager = new BiometricsManager(activity);
            /* Initialize object, meaning tell CredenceService to bind to this application. */
            App.BioManager.initializeBiometrics(new Biometrics.OnInitializedListener() {
                @Override
                public void onInitialized(Biometrics.ResultCode resultCode, String minimumVersion, String currentVersion) {

                    Log.e("OnInitialized",resultCode.name());
                    Log.d("OnInitialized", "Test App product name is " + App.BioManager.getProductName());

                    if (resultCode != Biometrics.ResultCode.OK) {
                        openBiometrics = true;
                        Log.d("Biometric's failed to initialize.","Initaliation failed");
                    }else{
                        openBiometrics = false;
                        Log.d("Biometric's starting.","Initaliation Successful");
                    }
                }
            });
        }else{
            Log.d("Biometric's starting.","IM ALREADY OPENED");
        }
    }

    public void notAllowed(View view){
        showMessage("Not Allowed to use this feature.");
    }




    protected enum InternalState {
        JUST_STARTING, WAITING_FOR_PERMISSION, READY
    }

    interface Extras {
        interface In {
            String REVERSE = "REVERSE";
        }
    }

    public interface Status {
        String OK = "OK";
        String ERROR = "ERROR";
        String CANCELADO = "CANCELADO";
        String PENDIENTE = "PENDIENTE";
        String NO_OK = "NO_OK";
    }

}
