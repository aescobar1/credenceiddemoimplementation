package com.example.democredenceid;

public interface ReturnCodeStructure {

    int getCode();

    String getDescription();
}
